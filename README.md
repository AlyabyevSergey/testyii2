Конфигурация Apache 2.4 + PHP 8.0 + MySQL 8.0

Скопировать все папки и файлы в папку.
После запуска скрипта (см. ниже) установить параметры подключения к базе данных в /config/db.php


Для создания и заполнения базы запустить скрипт:


-- Создание базы

CREATE DATABASE test_db1
CHARACTER SET utf8mb4
COLLATE utf8mb4_0900_ai_ci;

-- Создание таблицы Category
-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE test_db1;

--
-- Создать таблицу `category`
--
CREATE TABLE category (
  id int NOT NULL AUTO_INCREMENT,
  title varchar(255) NOT NULL,
  content text NOT NULL,
  category_id int DEFAULT NULL,
  slug varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Создать индекс `slug` для объекта типа таблица `category`
--
ALTER TABLE category
ADD UNIQUE INDEX slug (slug);

--
-- Создать внешний ключ
--
ALTER TABLE category
ADD CONSTRAINT FK_category_category_id FOREIGN KEY (category_id)
REFERENCES category (id);


-- Создание таблицы Post
-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE test_db1;

--
-- Создать таблицу `post`
--
CREATE TABLE post (
  id int NOT NULL AUTO_INCREMENT,
  title varchar(255) NOT NULL,
  content text NOT NULL,
  slug varchar(255) DEFAULT NULL,
  category_id int NOT NULL,
  file varchar(500) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_0900_ai_ci;

--
-- Создать индекс `slug` для объекта типа таблица `post`
--
ALTER TABLE post
ADD UNIQUE INDEX slug (slug);

--
-- Создать внешний ключ
--
ALTER TABLE post
ADD CONSTRAINT FK_post_category_id FOREIGN KEY (category_id)
REFERENCES category (id);


-- Заполнение таблицы Category
SET NAMES 'utf8';

INSERT INTO test_db1.category(id, title, content, category_id, slug) VALUES
(1, 'автомобили', 'тут про авто', NULL, '1'),
(2, 'KIA', 'тут только про KIA', 1, '2'),
(3, 'Домашние животные', 'тут все про домашних животных', NULL, '3'),
(4, 'Спорт', 'тут все про спорт', NULL, '4'),
(6, 'BMW', '11', 1, '121');

-- Заполнение таблицы Post

SET NAMES 'utf8';

INSERT INTO test_db1.post(id, title, content, slug, category_id, file) VALUES
(1, 'Первый пост про KIA33333', 'текст про первый пост234', '1', 2, 'upload/picture/kia.jpg'),
(2, 'пост про котов', 'коты вредные животные', '2', 3, 'upload/picture/cat.jpg'),
(6, 'BMW', 'Хорошая машниа', 'bcb48dddff8c14b5f452ee573b4db770', 6, 'upload/picture/2023-BMW-X7-2.jpg');
