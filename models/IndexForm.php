<?php


namespace app\models;

use yii\base\Model;

class IndexForm extends Model
{

    public $category_id;

    public function rules()
    {
        return [
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }
}