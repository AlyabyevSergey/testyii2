<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property int|null $category_id
 * @property string|null $slug
 *
 * @property Category $category
 * @property Category[] $categoties
 * @property Post[] $posts
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required', 'message' => 'required field'],
            [['title'], 'required', 'message' => 'required field'],
            [['content'], 'string'],
            [['category_id'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            ['slug', 'in', 'not' => true, 'strict' => true, 'range' => $this->getSlugList(), 'message' => 'Slug duplication'],
        ];
    }


    public static function getArrayIdCategory($category_id)
    {

        $category = Category::findOne($category_id);
        if ($category->category_id > 0) {
            return [$category_id];
        } else {
            $categories = Category::find()->where("category_id = $category_id")->asArray()->all();
            $result[] = $category_id;
            foreach ($categories as $_ar) {
                $result[] = $_ar["id"];
            }
            return $result;
        }
    }

    public static function getTree()
    {
        $categoryList = Category::find()->indexBy('id')->asArray()->all();
        $tree = [];
        foreach ($categoryList as $id => &$node) {
            if (!$node['category_id'])
                $tree[$id] = &$node;
            else
                $categoryList[$node['category_id']]['childs'][$node['id']] = &$node;
        }
        return $tree;
    }

    public static function getArrayForSelect()
    {
        $categoryTree = self::getTree();
        $categoryDropDownList = [];
        foreach ($categoryTree as $_ar) {
            $categoryDropDownList[] = ['id' => $_ar['id'], 'title' => $_ar['title']];
            if (isset($_ar["childs"])) {
                foreach ($_ar["childs"] as $child) {
                    $categoryDropDownList[] = ['id' => $child['id'], 'title' => "===    " . $child['title']];
                }
            }
        }
        return $categoryDropDownList;
    }


    public function getSlugList()
    {
        $result = [];
        $where = $this->id > 0 ? 'id <> ' . $this->id : '';
        $categoryList = Category::find()->where($where)->asArray()->all();
        foreach ($categoryList as $_ar) {
            $result[] = $_ar['slug'];
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'category_id' => 'Main Category',
            'slug' => 'Slug',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Categoties]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoties()
    {
        return $this->hasMany(Category::className(), ['category_id' => 'id']);
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['category_id' => 'id']);
    }
}
