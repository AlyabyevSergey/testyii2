<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property string|null $slug
 * @property int|null $category_id
 * @property string|null $file
 *
 * @property Category $category
 */
class Post extends \yii\db\ActiveRecord
{

    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required', 'message' => 'required field'],
            [['title'], 'required', 'message' => 'required field'],
            [['content'], 'string'],
            [['category_id'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['file'], 'string', 'max' => 500],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            ['slug', 'in', 'not' => true, 'strict' => true, 'range' => $this->getSlugList(), 'message' => 'Slug duplication'],
            [['image'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 2],

        ];
    }

    public function getSlugList()
    {
        $result = [];
        $where = $this->id > 0 ? 'id <> '.$this->id : '';
        $categoryList = Post::find()->where($where)->asArray()->all();
        foreach ($categoryList as $_ar) {
            $result[] = $_ar['slug'];
        }
        return $result;
    }


    public function upload() {
        $path = '';
        if ($this->validate()) {
            $path = 'upload/picture/' . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($path);
        }

        return $path;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'slug' => 'Slug',
            'category_id' => 'Category ID',
            'file' => 'File',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
