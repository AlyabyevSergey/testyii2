<?php

use app\models\Category;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
/** @var yii\web\View $this */

$this->registerCssFile("https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css");

$this->title = 'View Posts';
?>

<div class="site-index">
    <h1>View Posts</h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(Category::getArrayForSelect(), 'id', 'title'),
    ['prompt' => 'Change category'])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('View Posts', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <? if ($jsonPosts != '') {
        $posts = json_decode($jsonPosts, true); ?>
        <? foreach ($posts as $_ar) { ?>
            <a href="/site/item?slug=<?=$_ar["slug"]?>">
                <div class="card" style="width: 18rem;">
                    <?= Html::img(Url::to(['web/' . $_ar["file"], 't' => time()])) ?>
                    <div class="card-body">
                        <h5 class="card-title"><?= $_ar["title"] ?></h5>
                        <p class="card-text"><?= $_ar["content"] ?></p>
                    </div>
                </div>
            </a>
        <? } ?>
    <? } else { ?>
        <h2>No posts</h2>
    <? } ?>

</div>