<?php

use yii\helpers\Html;
use yii\helpers\Url;

$post = json_decode($jsonPost, true);
?>

    <div class="card" style="width: 18rem;">
        <?= Html::img(Url::to(['web/' . $post['file'], 't' => time()])) ?>
        <div class="card-body">
            <h5 class="card-title"><?= $post['title'] ?></h5>
            <p class="card-text"><?= $post['content'] ?></p>
        </div>
    </div>


